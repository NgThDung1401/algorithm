<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class SemiprimeFactorizationTest extends TestCase
{
    /**
     * @return void
     */
    public function testSemiprimeFactorization()
    {
        $this->assertEquals('2 3', $this->semiprimeFactorization(6));
        $this->assertEquals('3 5', $this->semiprimeFactorization(15));
        $this->assertEquals('5 7', $this->semiprimeFactorization(35));
        $this->assertEquals('7 11', $this->semiprimeFactorization(77));
        $this->assertEquals('3571 7213', $this->semiprimeFactorization(25757623));
        $this->assertEquals('27449 53507', $this->semiprimeFactorization(1468713643));
    }

    /**
     * 指定された数を因数分解し、小さい順に返す
     *
     * @param int $n
     * @return string
     */
    function semiprimeFactorization(int $n): string
    {
        for ($i = 2; $i < $n; $i++) {
            if (($n % $i == 0) && $this->checkIsPrime($i) && $this->checkIsPrime($n/$i) ) {
                $output = $i. " ". ($n/$i);
                return trim($output);
            }
        }
    }

    function checkIsPrime(int $n): bool{
        if ($n < 2) {
            return false;
        }

        for ($i = 2; $i <= sqrt($n); $i++) {
            if ($n % $i == 0) {
                return false;
            }
        }

        return true;
    }
}

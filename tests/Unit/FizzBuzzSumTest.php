<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class FizzBuzzSumTest extends TestCase
{
    /**
     * @return void
     */
    public function testFizzBuzzSum()
    {
        $this->assertEquals(60, $this->fizzBuzzSum(15));
        $this->assertEquals(266666333332, $this->fizzBuzzSum(1000000));
    }

    /**
     * 3 の倍数で Fizz 5 の倍数で Buzz 公倍数の場合 FizzBuzz とし作成した
     * FizzBuzz 列の n 項目までに含まれる数の和を返す
     *
     * @param int $n
     * @return int
     */
    function fizzBuzzSum(int $n): int
    {
        $sum = 0;
        $fizz = 3;
        $buzz = 5;

        for ($i = 1; $i <= $n; $i++) {
            if (($i % $fizz != 0) && ($i % $buzz != 0)) {
                $sum += $i;
            }
        }

        return $sum;
    }
}

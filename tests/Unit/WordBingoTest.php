<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class WordBingoTest extends TestCase
{
    const PATTERN1 = <<<__LONG_TEXT__
3
apple orange cube
batch web cloud
sql http https
7
web
https
windows
batch
keyboard
apple
cpu
__LONG_TEXT__;

    const PATTERN2 = <<<__LONG_TEXT__
3
cpp kotlin typescript
csharp ruby php
go rust dart
5
java
delphi
fortran
haskell
python
__LONG_TEXT__;


    const PATTERN3 = <<<__LONG_TEXT__
4
beer wine gin vodka
beef chicken pork seafood
ant bee ladybug beetle
bear snake dog camel
7
be
bear
bee
beef
been
beer
beetle
__LONG_TEXT__;

    /**
     * @return void
     */
    public function testWordBingo()
    {
        // 左上から右下にかけてななめに3個の印が並び、ビンゴが成立しています。
        $this->assertEquals('yes', $this->wordBingo(self::PATTERN1));
        // 印は1つも付いていません。
        $this->assertEquals('no', $this->wordBingo(self::PATTERN2));
        // 4個の印が並ぶ列はありませんので、ビンゴは成立していません。
        $this->assertEquals('no', $this->wordBingo(self::PATTERN3));
    }

    /**
     * @param string $input
     * @return string
     */
    function wordBingo(string $input): string
    {
        $output = 'no';
        $bingo_table = array();
        $word_list = array();
        list($bingo_table, $word_list) = $this->readData($input);

        if ($this->checkIsBingo($bingo_table, $word_list)) return 'yes';

        return $output;
    }

    function readData(string $input) {
        $data_array = explode("\n", $input);

        $bingo_table = array();
        $word_list = array();

        for ($i = 1; $i <= $data_array[0]; $i++) {
            array_push($bingo_table, explode(" ", $data_array[$i]));
        }


        for ($i = $data_array[0] + 2; $i < count($data_array); $i++) {
            array_push($word_list, $data_array[$i]);
        }

        return array($bingo_table, $word_list);
    }

    function checkIsBingo($bingo_table, $word_list): bool {
        // Size of bingo table (n x n)
        $n = count($bingo_table);

        // The number of the selected items in a row
        $count = 0;

        // check horizontal line bingo
        for ($i = 0; $i < $n; $i++) {
            $count = 0;
            for ($j = 0; $j < $n; $j++) {
                $be_in_array = in_array($bingo_table[$i][$j], $word_list);

                if ($be_in_array) {
                    $count++;
                } else {
                    break;
                }
            }

            if($count == $n) return true;
        }

        // check vertical line bingo
        for ($i = 0; $i < $n; $i++) {
            $count = 0;
            for ($j = 0; $j < $n; $j++) {
                $be_in_array = in_array($bingo_table[$j][$i], $word_list);

                if ($be_in_array) {
                    $count++;
                } else {
                    break;
                }
            }

            if($count == $n) return true;
        }

        // check cross line bingo
        $count = 0;
        for ($i = 0; $i < $n; $i++) {
            $be_in_array = in_array($bingo_table[$i][$i], $word_list);

            if ($be_in_array) {
                $count++;
            } else {
                break;
            }

            if($count == $n) return true;
        }

        // check cross line bingo
        $count = 0;
        for ($i = 0; $i < $n; $i++) {
            $be_in_array = in_array($bingo_table[$i][($n-1)-$i], $word_list);

            if ($be_in_array) {
                $count++;
            } else {
                break;
            }

            if($count == $n) return true;
        }

        return false;
    }
}

<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class SumOfPrimesTest extends TestCase
{
    /**
     * @return void
     */
    public function testSumOfPrimes()
    {
        $this->assertEquals(2, $this->sumOfPrimes(1));
        $this->assertEquals(5, $this->sumOfPrimes(2));
        $this->assertEquals(28, $this->sumOfPrimes(5));
        $this->assertEquals(129, $this->sumOfPrimes(10));
        $this->assertEquals(3682913, $this->sumOfPrimes(1000));
        $this->assertEquals(16274627, $this->sumOfPrimes(2000));
    }

    /**
     * 指定された数の素数を合計して返す
     * 素数はかならず 2 から値の小さい順に加算する
     *
     * @param int $n
     * @return int
     */
    function sumOfPrimes(int $n): int
    {
        $sum = 0;
        $count = 1;
        $current_num = 2;

        while($count <= $n) {
            if ($this->checkIsPrime($current_num)) {
                $sum += $current_num;
                $count++;
            }
            $current_num++;
        }

        return $sum;
    }

    function checkIsPrime(int $n): bool{
        if ($n < 2) {
            return false;
        }

        for ($i = 2; $i <= sqrt($n); $i++) {
            if ($n % $i == 0) {
                return false;
            }
        }

        return true;
    }
}

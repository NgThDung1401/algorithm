<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class SequenceGeneratorTest extends TestCase
{
    /**
     * @return void
     */
    public function testSequenceGenerator()
    {
        // PREFIX から始まる連番を生成
        /** @var SequenceGenerator $firstSeq */
        $firstSeq = SequenceGenerator::getInstance();

        $this->assertEquals('Tests\Unit\SequenceGenerator', get_class($firstSeq));

        $this->assertEquals(
            ['PREFIX-1', 'PREFIX-2', 'PREFIX-3', 'PREFIX-4', 'PREFIX-5'],
            $firstSeq->take('PREFIX', 5)
        );
        $this->assertEquals(
            ['PREFIX-6', 'PREFIX-7', 'PREFIX-8'],
            $firstSeq->take('PREFIX', 3)
        );

        // CODE から始まる連番を生成
        $this->assertEquals(
            ['CODE-1', 'CODE-2', 'CODE-3', 'CODE-4', 'CODE-5']
            , SequenceGenerator::take('CODE', 5)
        );

        // 再度 PREFIX から始まる連番を生成
        /** @var SequenceGenerator $secondSeq */
        $secondSeq = SequenceGenerator::getInstance();
        $this->assertEquals(
            ['PREFIX-9', 'PREFIX-10', 'PREFIX-11', 'PREFIX-12'],
            $secondSeq->take('PREFIX', 4)
        );
        $this->assertEquals(
            ['PREFIX-13', 'PREFIX-14'],
            $firstSeq->take('PREFIX', 2)
        );
    }
}

class SequenceGenerator
{
    private static $_instance;

    private static $data = [];

    function __construct() {
        self::$_instance = $this;
    }

    public static function getInstance(): SequenceGenerator {
        if (!( self::$_instance instanceof self) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function take($prefix, $number): array {
        $output = [];

        if (array_key_exists($prefix, self::$data)) {
            $current_value = self::$data[$prefix];

            for($i = 1; $i <= $number; $i++) {
                array_push($output, $prefix. "-". ($current_value + $i));
            }

            self::$data[$prefix] += $number;
        } else {
            for($i = 1; $i <= $number; $i++) {
                array_push($output, $prefix. "-". $i);
            }

            self::$data[$prefix] = $number;
        }
        return $output;
    }
}
